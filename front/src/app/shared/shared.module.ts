import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoaderComponent } from './loader/loader.component';
import { ErrorMessagesComponent } from './error-messages/error-messages.component';

@NgModule({
  imports: [CommonModule],
  declarations: [LoaderComponent, ErrorMessagesComponent],
  exports: [LoaderComponent, ErrorMessagesComponent]
})
export class SharedModule {}
