import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { CredentialsService, Logger } from '@app/core';
import { intersection } from 'lodash-es';
import { Observable } from 'rxjs';

const logger = new Logger('RoleGuard');

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {
  constructor(private credentialService: CredentialsService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const roles = this.credentialService.credentials.data.roles;

    if (!!intersection(roles, route.data.roles).length) {
      return true;
    }
    this.router.navigate(['/404']);
    return false;
  }
}
