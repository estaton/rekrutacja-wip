import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthenticationService, Logger } from '@app/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

const logger = new Logger('HttpErrorInterceptor');

export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private router: Router, private authenticationService: AuthenticationService) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req)
      .pipe(catchError((error: HttpErrorResponse) => {
        /**
         * api zwróciło Unauthorized, albo użytkownik nie był zalogowany albo sesja wygasła
         * zostanie więc przekierowany na stronę logowania
         */
        if(error.status === 401) {
          logger.info('sesja nieaktywna, przekierowanie do logowania');
          this.authenticationService.logout().subscribe(() => {
            this.router.navigate(['/login']);
          });
        }
        return throwError(error);
      }))
  }
}
