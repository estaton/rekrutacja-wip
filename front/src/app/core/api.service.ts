import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserModel } from '@app/core/api/user-model';
import { Credentials } from '@app/core/authentication/credentials.service';
import { Logger } from '@app/core/logger.service';
import { environment } from '@env/environment';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

const logger = new Logger('ApiService');

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  protected cachedPositionDictionary: any;

  constructor(private http: HttpClient) {
  }

  login(username: string, password: string) {
    return this.http.post<Credentials>('/login', {username, password});
  }

  getPositionDictionary(): Observable<any> {
    if (this.cachedPositionDictionary) {
      logger.info('position dictionary was loaded from cache');
      return of(this.cachedPositionDictionary);
    }
    return this.http.get(`${environment.serverUrl}/dictionary/position`).pipe(
      map(data => {
        this.cachedPositionDictionary = data;
        return data;
      })
    );
  }

  browseUsers(): Observable<Array<UserModel>> {
    return this.http.get<Array<UserModel>>('/user');
  }

  addUser(data: UserModel) {
    return this.http.post('/user', data);
  }

  editUser(id: string, data: UserModel) {
    return this.http.put(`/user/${id}`, data);
  }

  deleteUser(id: string): Observable<void> {
    return this.http.delete<void>(`/user/${id}`);
  }
}
