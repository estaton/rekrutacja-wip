export class UserModel {
  id: string;
  email: string;
  firstname: string;
  surname: string;
  description: string;
  position: {
    id: string;
    name: string;
    code: string;
  };
  additionalData: {};
}
