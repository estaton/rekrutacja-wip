import { Component, OnInit } from '@angular/core';
import { Logger } from '@app/core';
import { ApiService } from '@app/core/api.service';
import { UserModel } from '@app/core/api/user-model';

const logger = new Logger('UserBrowseComponent');

@Component({
  selector: 'app-user-browse',
  templateUrl: './user-browse.component.html',
  styleUrls: ['./user-browse.component.scss']
})
export class UserBrowseComponent implements OnInit {
  page: number = 1;
  users: Array<UserModel>;
  activeUser: UserModel;

  constructor(protected apiService: ApiService) {}

  ngOnInit() {
    this.getData();
  }

  protected getData() {
    this.apiService.browseUsers().subscribe(users => (this.users = users));
  }

  remove(index: number) {
    if (confirm(`Czy napewno usunąc użytkownika ${this.users[index].email}?`)) {
      this.apiService.deleteUser(this.users[index].id).subscribe(() => {
        this.users.splice(index, 1);
      });
    }
  }

  edit(index: number) {
    this.activeUser = this.users[index];
  }

  refresh() {
    this.activeUser = null;
    this.getData();
  }
}
