import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { NgbAlertModule, NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { UserAddComponent } from './user-add/user-add.component';
import { UserBrowseComponent } from './user-browse/user-browse.component';

@NgModule({
  declarations: [UserAddComponent, UserBrowseComponent],
  exports: [UserAddComponent],
  imports: [CommonModule, ReactiveFormsModule, TranslateModule, SharedModule, NgbAlertModule, NgbPaginationModule]
})
export class UserModule {}
