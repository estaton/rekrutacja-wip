import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Logger } from '@app/core';
import { ApiService } from '@app/core/api.service';
import { UserModel } from '@app/core/api/user-model';
import { flow, get, join, map as _map, partialRight, toPath } from 'lodash-es';
import { finalize } from 'rxjs/operators';

const logger = new Logger('UserAddComponent');

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent implements OnInit {
  @Input() user: UserModel;
  @Output() onSaveSuccessful = new EventEmitter<void>();
  form: FormGroup;
  visibleAdditionalData: Map<string, boolean> = new Map<string, boolean>();
  readonly positionDeveloper = 'position-developer';
  readonly positionTester = 'position-tester';
  readonly positionProjectManager = 'position-project-manager';
  isFormSubmitted = false;
  mappedPositions: Map<string, any> = new Map<string, any>();
  isSaveSuccessful = false;

  constructor(protected fb: FormBuilder, protected apiService: ApiService) {
    this.createForm();
  }

  /**
   * zwraca listę błędów z kontrolki przy użyciu przyjaźniejszej formy "groupA.groupB.controlC"
   * @param fieldPath
   */
  getErrors(fieldPath: string) {
    let path = flow(
      toPath,
      partialRight(_map, (position: string) => `controls.${position}`),
      join
    )(fieldPath);

    let control = get(this.form, path);
    if (control) {
      return control.touched ? control.errors : null;
    }
    return null;
  }

  ngOnInit() {
    this.apiService.getPositionDictionary().subscribe(positions => {
      const positionsMap = new Map();
      positions.forEach((position: any) => {
        positionsMap.set(position.code, position);
      });
      this.mappedPositions = positionsMap;
    });

    /**
     * jeśli dane przyszły z input należy zaktualizować formularz
     */
    if (this.user) {
      this.updateFormWithData(this.user);
    }
  }

  /**
   * aktualizuje formularz na podstawie przesłanego modelu użytkownika
   * @param user
   */
  updateFormWithData(user: UserModel) {
    let userData = {
      email: user.email,
      firstname: user.firstname,
      surname: user.surname,
      description: user.description,
      position: user.position.code,
      additionalData: user.additionalData
    };
    /**
     * ze względu na specyficzną logikę stanowiska należy je wcześniej ustawić, aby określić widoczność dodatkowych pól
     */
    this.form.get('position').patchValue(userData.position);
    this.setAdditionalData();
    this.form.patchValue(userData);
  }

  createForm() {
    this.form = this.fb.group({
      email: ['', Validators.required],
      firstname: ['', Validators.required],
      surname: ['', Validators.required],
      description: [''],
      position: ['', Validators.required],
      additionalData: this.fb.group({})
    });
    this.setAdditionalData();
  }

  setAdditionalData() {
    /**
     * przygotowanie definicji dodatkowych pól formularza
     */
    const additionalData = new Map([
      ['knownIDE', { on: [this.positionDeveloper], control: new FormControl('', Validators.required) }],
      [
        'knownProgrammingLanguages',
        { on: [this.positionDeveloper], control: new FormControl('', Validators.required) }
      ],
      ['isMysqlKnown', { on: [this.positionDeveloper], control: new FormControl(false, Validators.required) }],
      [
        'knownProjectManagementMethods',
        {
          on: [this.positionProjectManager],
          control: new FormControl('', Validators.required)
        }
      ],
      [
        'knownReportingSystems',
        {
          on: [this.positionProjectManager, this.positionTester],
          control: new FormControl('', Validators.required)
        }
      ],
      ['isScrumKnown', { on: [this.positionProjectManager], control: new FormControl(false, Validators.required) }],
      ['knownTestingSystems', { on: [this.positionTester], control: new FormControl('', Validators.required) }],
      ['isSeleniumKnown', { on: [this.positionTester], control: new FormControl(false, Validators.required) }]
    ]);
    const position = this.form.get('position').value;
    const additionalDataForm = this.form.get('additionalData') as FormGroup;

    additionalData.forEach((control, fieldName) => {
      if (~control.on.indexOf(position)) {
        additionalDataForm.addControl(fieldName, control.control);
        this.visibleAdditionalData.set(fieldName, true);
      } else {
        additionalDataForm.removeControl(fieldName);
        this.visibleAdditionalData.set(fieldName, false);
      }
    });
  }

  onSubmit() {
    this.isFormSubmitted = true;
    const data = this.form.value;
    data.position = this.mappedPositions.get(data.position).id;

    let apiAction;
    if (this.user) {
      apiAction = this.apiService.editUser(this.user.id, data);
    } else {
      apiAction = this.apiService.addUser(data);
    }

    apiAction
      .pipe(
        finalize(() => {
          this.isFormSubmitted = false;
        })
      )
      .subscribe(
        () => {
          this.form.reset();
          this.isSaveSuccessful = true;
          this.onSaveSuccessful.emit();
        },
        data => {
          if (data.error.error === 'validation_error') {
            this.handleErrorMessages(data.error.details, this.form);
          }
        }
      );
  }

  /**
   * obsługuje błędy zwracane przez API
   * @param errors
   * @param formGroup
   */
  handleErrorMessages(errors: any, formGroup: FormGroup) {
    Object.keys(errors).forEach(fieldName => {
      let errorList = {};
      if (Array.isArray(errors[fieldName])) {
        errors[fieldName].forEach((error: string) => {
          errorList[error] = true;
        });
        let formControl = formGroup.get(fieldName);
        if (formControl) {
          formControl.setErrors(errorList);
          formControl.markAsTouched();
        }
      } else {
        this.handleErrorMessages(errors[fieldName], formGroup.get(fieldName) as FormGroup);
      }
    });
  }

  hideAlert() {
    this.isSaveSuccessful = false;
  }
}
