import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserModule } from '@app/user/user.module';
import { TranslateModule } from '@ngx-translate/core';

import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [CommonModule, TranslateModule, CoreModule, SharedModule, HomeRoutingModule, UserModule],
  declarations: [HomeComponent],
  providers: []
})
export class HomeModule {}
