import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ErrorNotfoundComponent } from '@app/error/error-notfound/error-notfound.component';
import { ErrorUnexpectedComponent } from '@app/error/error-unexpected/error-unexpected.component';
import { Shell } from '@app/shell/shell.service';

const routes: Routes = [
  Shell.childRoutes([
    {path: '404', component: ErrorNotfoundComponent},
    {path: 'unexpected', component: ErrorUnexpectedComponent}
  ]),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErrorRoutingModule { }
