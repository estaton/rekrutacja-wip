import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ErrorRoutingModule } from './error-routing.module';
import { ErrorNotfoundComponent } from './error-notfound/error-notfound.component';
import { ErrorUnexpectedComponent } from './error-unexpected/error-unexpected.component';

@NgModule({
  declarations: [ErrorNotfoundComponent, ErrorUnexpectedComponent],
  imports: [
    CommonModule,
    ErrorRoutingModule
  ]
})
export class ErrorModule { }
