import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard } from '@app/core';
import { RoleGuard } from '@app/core/role.guard';

import { Shell } from '@app/shell/shell.service';
import { UserBrowseComponent } from '@app/user/user-browse/user-browse.component';

const routes: Routes = [
  Shell.childRoutes([
    {
      path: 'admin',
      component: UserBrowseComponent,
      canActivate: [AuthenticationGuard, RoleGuard],
      data: { roles: ['ROLE_ADMIN'] }
    }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class AdminRoutingModule {}
