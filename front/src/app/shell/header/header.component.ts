import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService, CredentialsService } from '@app/core';
import { intersection } from 'lodash-es';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  menuHidden = true;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private credentialsService: CredentialsService
  ) {}

  get username(): string | null {
    const credentials = this.credentialsService.credentials;
    return credentials ? credentials.data.username : null;
  }

  get isAuthenticated(): boolean {
    return this.credentialsService.isAuthenticated();
  }

  ngOnInit() {}

  toggleMenu() {
    this.menuHidden = !this.menuHidden;
  }

  logout() {
    this.authenticationService.logout().subscribe(() => this.router.navigate(['/'], { replaceUrl: true }));
  }

  isGranted(roles: Array<string> | string) {
    if (!this.credentialsService.isAuthenticated()) return false;
    return !!intersection(this.credentialsService.credentials.data.roles, [].concat(roles)).length;
  }
}
