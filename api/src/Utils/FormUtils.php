<?php


namespace App\Utils;


use Symfony\Component\Form\FormInterface;

class FormUtils
{
    public static function getErrorsFromForm(FormInterface $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $payload = $error->getCause()->getConstraint()->payload;
            if(!is_null($payload['code'])) {
                $errors[] = $payload['code'];
            } else {
                $errors[] = $error->getMessage();
            }
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = static::getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }
}
