<?php

namespace App\Repository;

use App\Entity\UserAdditionalData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserAdditionalData|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserAdditionalData|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserAdditionalData[]    findAll()
 * @method UserAdditionalData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserAdditionalDataRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserAdditionalData::class);
    }

    // /**
    //  * @return UserAdditionalData[] Returns an array of UserAdditionalData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserAdditionalData
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
