<?php

namespace App\Form;

use App\Entity\UserAdditionalData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserAdditionalDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('knownIDE')
            ->add('knownProgrammingLanguages')
            ->add('isMysqlKnown')
            ->add('knownProjectManagementMethods')
            ->add('knownReportingSystems')
            ->add('isScrumKnown')
            ->add('knownTestingSystems')
            ->add('isSeleniumKnown')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserAdditionalData::class,
        ]);
    }
}
