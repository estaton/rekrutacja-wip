<?php

namespace App\Form;

use App\Entity\Position;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('firstname')
            ->add('surname')
            ->add('description')
            ->add('position')
            ->add('additionalData', UserAdditionalDataType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'        => User::class,
            "validation_groups" => function (FormInterface $form) {
                $commonGroups = ['registration'];
                /**
                 * @var $userPosition Position
                 */
                $userPosition = $form->getData()->getPosition();
                if (is_null($userPosition)) {
                    return $commonGroups;
                }
                $group = [
                    Position::DEVELOPER => ['developer'],
                    Position::TESTER => ['tester'],
                    Position::PROJECT_MANAGER => ['project-manager'],
                ][(string)$userPosition->getCode()];
                $groups = array_merge($commonGroups, $group);
                return $groups;
            },
        ]);
    }
}
