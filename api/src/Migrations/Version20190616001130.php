<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\Position;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190616001130 extends AbstractMigration implements ContainerAwareInterface
{
    private $container;
    public function getDescription() : string
    {
        return '';
    }

    public function postUp(Schema $schema) : void
    {
        $manager = $this->container->get('doctrine.orm.entity_manager');

        $positions = [
            ['code' => Position::TESTER, 'name' => 'Tester'],
            ['code' => Position::DEVELOPER, 'name' => 'Developer'],
            ['code' => Position::PROJECT_MANAGER, 'name' => 'Project Manager']
        ];

        foreach ($positions as $position) {
            $positionEntity = new Position();
            $positionEntity->setCode($position['code']);
            $positionEntity->setName($position['name']);
            $positionEntity->setIsSystem(true);
            $manager->persist($positionEntity);
            unset($positionEntity);
        }

        $manager->flush();

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    /**
     * Sets the container.
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function up(Schema $schema): void
    {
        // TODO: Implement up() method.
    }
}
