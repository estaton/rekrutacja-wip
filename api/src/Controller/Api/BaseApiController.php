<?php


namespace App\Controller\Api;


use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

abstract class BaseApiController extends AbstractController
{
    protected $logger;
    protected $normalizer;
    public function __construct(LoggerInterface $logger, NormalizerInterface $normalizer)
    {
        $this->logger = $logger;
        $this->normalizer = $normalizer;
    }

    /**
     * @return NormalizerInterface
     */
    public function getNormalizer(): NormalizerInterface
    {
        return $this->normalizer;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    public function jsonError(string $errorMessage, $httpStatus = Response::HTTP_BAD_REQUEST) {
        return $this->json([
            'error' => $errorMessage
        ], $httpStatus);
    }

    public function jsonValidationError(array $validationErrors) {
        return $this->json([
            'error' => 'validation_error',
            'details' => $validationErrors
        ], Response::HTTP_BAD_REQUEST);
    }

    public function jsonUnexpectedError() {
        return $this->json(['error' => 'unexpected error'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function getJsonContent(Request $request) {
        return json_decode($request->getContent(), true);
    }

    public function getEM() {
        return $this->getDoctrine()->getManager();
    }
}
