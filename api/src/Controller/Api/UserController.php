<?php

namespace App\Controller\Api;

use App\Entity\Position;
use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Service\RegistrationMailerService;
use App\Utils\FormUtils;
use DateTime;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class UserController extends BaseApiController
{

    /**
     * @Route("/api/user", name="api_user_add", methods={"POST"})
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param RegistrationMailerService $registrationMailerService
     * @return JsonResponse
     * @throws Exception
     */
    public function add(Request $request, UserPasswordEncoderInterface $passwordEncoder, RegistrationMailerService $registrationMailerService): JsonResponse
    {
        $data = $this->getJsonContent($request);

        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->submit($data);

        if (!$form->isValid()) {
            return $this->jsonValidationError(FormUtils::getErrorsFromForm($form));
        } else {
            $user->setPassword($passwordEncoder->encodePassword($user, sha1(random_bytes(32))));
            $this->getEM()->persist($user);
            $this->getEM()->flush();

            $mailData = $this->normalizeUser($user);
            $registrationMailerService->sendConfirmation($mailData);
            return $this->getNormalizedJsonResponse($user, Response::HTTP_CREATED);
        }

    }

    /**
     * @param User|User[] $users
     * @return array|bool|float|int|string
     * @throws Exception
     */
    protected function normalizeUser($users)
    {
        /**
         * jeśli na wejściu dostaliśmy zbiór danych należy je obrobić indywidualnie, że względu na mapowanie dto w zależności od stanowiska
         */
        if (is_array($users)) {
            $normalizedUsers = array_map(function ($user) {
                return $this->normalizeUser($user);
            }, $users);
            return $normalizedUsers;
        }

        /**
         * wprowadzono pojedynczy element, weryfikacja typu
         */
        if (!$users instanceof User) {
            throw new Exception('unsupported type');
        }
        try {
            /**
             * wygeneruj model dto odpowiedzi api z wykorzystaniem grup zależnych od stanowiska
             */
            return $this->getNormalizer()->normalize(
                $users,
                'array',
                ['groups' => ['dto-model',
                              [Position::TESTER          => 'dto-model-tester',
                               Position::PROJECT_MANAGER => 'dto-model-project-manager',
                               Position::DEVELOPER       => 'dto-model-developer',][$users->getPosition()->getCode()]]]
            );
        } catch (ExceptionInterface $e) {
            $this->getLogger()->critical($e->getMessage());
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param User|User[] $user
     * @param int $http_status
     * @return JsonResponse
     */
    protected function getNormalizedJsonResponse($user, $http_status = Response::HTTP_OK)
    {
        try {
            return $this->json($this->normalizeUser($user), $http_status);
        } catch (Exception $e) {
            return $this->jsonUnexpectedError();
        }
    }

    /**
     * @Route("/api/user", name="api_user_browse", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     * @return JsonResponse
     */
    public function browse()
    {
        /**
         * @var $repository UserRepository
         */
        $repository = $this->getEM()->getRepository(User::class);
        $users = $repository->findAllActive();
        return $this->getNormalizedJsonResponse($users);
    }

    /**
     * @param User $user
     * @return JsonResponse
     * @Route("api/user/{id}", name="api_user_read", methods={"GET"})
     * @Cache(lastModified="user.getUpdatedAt()", Etag="'User' ~ user.getId() ~ user.getUpdatedAt().getTimestamp()")
     * @IsGranted("ROLE_ADMIN")
     */
    public function read(User $user)
    {
        try {
            return $this->getNormalizedJsonResponse($user);
        } catch (Exception $e) {
            return $this->jsonUnexpectedError();
        }
    }

    /**
     * @param User $user
     *
     * @param Request $request
     * @return JsonResponse
     * @Route("api/user/{id}", methods={"PUT"}, name="api_user_edit")
     * @IsGranted("ROLE_ADMIN")
     */
    public function edit(User $user, Request $request)
    {
        $form = $this->createForm(UserType::class, $user);

        $data = $this->getJsonContent($request);

        /**
         * nie usuwaj brakujących danych
         * pozwala aktualizować część danych bez potrzeby wysyłania wszystkich
         */
        $form->submit($data, false);

        if (!$form->isValid()) {
            return $this->jsonValidationError(FormUtils::getErrorsFromForm($form));
        } else {
            $this->getEM()->persist($user);
            $this->getEM()->flush();
            return $this->getNormalizedJsonResponse($user);
        }
    }

    /**
     * @param User $user
     *
     * @return Response
     * @throws Exception
     * @Route("api/user/{id}", methods={"DELETE"}, name="api_user_delete")
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(User $user)
    {
        /**
         * soft delete
         */
        $user->setDeletedAt(new DateTime());
        $this->getEM()->persist($user);
        $this->getEM()->flush();

        return new Response(null, Response::HTTP_OK);
    }
}
