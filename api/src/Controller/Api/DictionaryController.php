<?php

namespace App\Controller\Api;

use App\Entity\Dictionary;
use App\Entity\Position;
use App\Repository\DictionaryRepository;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class DictionaryController extends BaseApiController
{
    /**
     * @Route("/api/dictionary/{type}", name="api_dictionary_browse", methods={"GET"})
     * @param string $type
     * @return JsonResponse
     */
    public function browse(string $type): JsonResponse
    {
        /**
         * obsługa typów słowników
         * aktualnie dostępny jedynie Position
         */
        switch ((string)$type) {
            case Position::PREFIX:
                $repoClass = Position::class;
                break;
            default:
                return $this->jsonError('unknown dictionary type', Response::HTTP_BAD_REQUEST);
        }

        /**
         * @var $repository DictionaryRepository
         */
        $repository = $this->getEM()->getRepository($repoClass);
        $dictionaries = $repository->findAll();
        try {
            return $this->json($this->normalizeDictionary($dictionaries));
        } catch (Exception $e) {
            return $this->jsonUnexpectedError();
        }
    }

    /**
     * @param Dictionary|Dictionary[] $dictionaries
     * @return array|bool|float|int|string
     * @throws HttpException
     */
    public function normalizeDictionary($dictionaries)
    {
        try {
            return $this->getNormalizer()->normalize($dictionaries, 'array', ['groups' => 'dto-model']);
        } catch (ExceptionInterface $e) {
            $this->getLogger()->critical($e->getMessage());
            throw new HttpException($e->getMessage());
        }
    }
}
