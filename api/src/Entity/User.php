<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, groups={"registration"})
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type")
 */
class User extends BaseEntity implements UserInterface
{

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\Email(groups={"registration"},payload={"code"="email"})
     * @Assert\NotBlank(groups={"registration"},payload={"code"="notblank"})
     * @Groups({"dto-model", "dto-model-developer"})
     */
    protected $email;

    /**
     * @ORM\Column(type="json")
     */
    protected $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    protected $password;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\NotBlank(groups={"registration"},payload={"code"="notblank"})
     * @Groups({"dto-model", "dto-model-developer"})
     */
    protected $firstname;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\NotBlank(groups={"registration"},payload={"code"="notblank"})
     * @Groups({"dto-model", "dto-model-developer"})
     */
    protected $surname;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"dto-model", "dto-model-developer"})
     */
    protected $description;
    /**
     * @ORM\ManyToOne(targetEntity="Position")
     * @Groups({"dto-model", "dto-model-developer"})
     */
    protected $position;
    /**
     * @var UserAdditionalData
     * @ORM\OneToOne(targetEntity="UserAdditionalData", cascade={"persist"})
     * @Assert\NotBlank(groups={"registration"},payload={"code"="notblank"})
     * @Assert\Valid()
     * @Groups({"dto-model", "dto-model-developer"})
     */
    protected $additionalData;

    /**
     * @return UserAdditionalData
     */
    public function getAdditionalData(): ?UserAdditionalData
    {
        return $this->additionalData;
    }

    /**
     * @param UserAdditionalData $additionalData
     * @return User
     */
    public function setAdditionalData(UserAdditionalData $additionalData): User
    {
        $this->additionalData = $additionalData;
        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return (string)$this->firstname;
    }

    /**
     * @param string $firstname
     * @return User
     */
    public function setFirstname(?string $firstname): User
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return (string)$this->surname;
    }

    /**
     * @param string $surname
     * @return User
     */
    public function setSurname(?string $surname): User
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPosition(): ?Position
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     * @return User
     */
    public function setPosition(?Position $position): self
    {
        $this->position = $position;
        return $this;
    }
}
