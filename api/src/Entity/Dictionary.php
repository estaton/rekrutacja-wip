<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DictionaryRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="dictionary", type="string")
 */

class Dictionary extends BaseEntity
{

    /**
     * @ORM\Column(type="string", unique=true)
     * @Groups({"dto-model", "dto-model-developer"})
     */
    protected $code;
    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"dto-model", "dto-model-developer"})
     */
    private $name;
    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isSystem = false;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getIsSystem(): ?bool
    {
        return $this->isSystem;
    }

    public function setIsSystem(bool $isSystem): self
    {
        $this->isSystem = $isSystem;

        return $this;
    }
}
