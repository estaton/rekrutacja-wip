<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserAdditionalDataRepository")
 */
class UserAdditionalData extends BaseEntity
{

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(groups={"developer"},payload={"code"="notblank"})
     * @Groups({"dto-model-developer"})
     */
    private $knownIDE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(groups={"developer"},payload={"code"="notblank"})
     * @Groups({"dto-model-developer"})
     */
    private $knownProgrammingLanguages;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Assert\NotNull(groups={"developer"},payload={"code"="notnull"})
     * @Groups({"dto-model-developer"})
     */
    private $isMysqlKnown;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(groups={"project-manager"},payload={"code"="notblank"})
     * @Groups({"dto-model-project-manager"})
     */
    private $knownProjectManagementMethods;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(groups={"project-manager", "tester"},payload={"code"="notblank"})
     * @Groups({"dto-model-project-manager", "dto-model-tester"})
     */
    private $knownReportingSystems;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Assert\NotNull(groups={"project-manager"},payload={"code"="notnull"})
     * @Groups({"dto-model-project-manager"})
     */
    private $isScrumKnown;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(groups={"tester"},payload={"code"="notblank"})
     * @Groups({"dto-model-tester"})
     */
    private $knownTestingSystems;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Assert\NotNull(groups={"tester"},payload={"code"="notnull"})
     * @Groups({"dto-model-tester"})
     */
    private $isSeleniumKnown;

    public function getKnownIDE(): ?string
    {
        return $this->knownIDE;
    }

    public function setKnownIDE(?string $knownIDE): self
    {
        $this->knownIDE = $knownIDE;

        return $this;
    }

    public function getKnownProgrammingLanguages(): ?string
    {
        return $this->knownProgrammingLanguages;
    }

    public function setKnownProgrammingLanguages(string $knownProgrammingLanguages): self
    {
        $this->knownProgrammingLanguages = $knownProgrammingLanguages;

        return $this;
    }

    public function getIsMysqlKnown(): ?bool
    {
        return $this->isMysqlKnown;
    }

    public function setIsMysqlKnown(?bool $isMysqlKnown): self
    {
        $this->isMysqlKnown = $isMysqlKnown;

        return $this;
    }

    public function getKnownProjectManagementMethods(): ?string
    {
        return $this->knownProjectManagementMethods;
    }

    public function setKnownProjectManagementMethods(?string $knownProjectManagementMethods): self
    {
        $this->knownProjectManagementMethods = $knownProjectManagementMethods;

        return $this;
    }

    public function getKnownReportingSystems(): ?string
    {
        return $this->knownReportingSystems;
    }

    public function setKnownReportingSystems(?string $knownReportingSystems): self
    {
        $this->knownReportingSystems = $knownReportingSystems;

        return $this;
    }

    public function getIsScrumKnown(): ?bool
    {
        return $this->isScrumKnown;
    }

    public function setIsScrumKnown(?bool $isScrumKnown): self
    {
        $this->isScrumKnown = $isScrumKnown;

        return $this;
    }

    public function getKnownTestingSystems(): ?string
    {
        return $this->knownTestingSystems;
    }

    public function setKnownTestingSystems(?string $knownTestingSystems): self
    {
        $this->knownTestingSystems = $knownTestingSystems;

        return $this;
    }

    public function getIsSeleniumKnown(): ?bool
    {
        return $this->isSeleniumKnown;
    }

    public function setIsSeleniumKnown(?bool $isSeleniumKnown): self
    {
        $this->isSeleniumKnown = $isSeleniumKnown;

        return $this;
    }
}
