<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PositionRepository")
 */
class Position extends Dictionary
{
    const PREFIX = 'position';

    const DEVELOPER = self::PREFIX . "-developer";
    const TESTER = self::PREFIX . "-tester";
    const PROJECT_MANAGER = self::PREFIX . "-project-manager";
}
