<?php


namespace App\Service;


use Exception;
use Psr\Log\LoggerInterface;
use Swift_Mailer;
use Swift_Message;
use Twig\Environment;

class RegistrationMailerService
{
    /**
     * @var Swift_Mailer
     */
    protected $mailer;
    /**
     * @var Swift_Message
     */
    protected $message;

    protected $twig;
    protected $logger;

    public function __construct(Swift_Mailer $mailer, Environment $twig, LoggerInterface $logger)
    {
        $this->twig = $twig;
        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    /**
     * @param $mailData
     * @throws Exception
     */
    public function sendConfirmation($mailData)
    {
        $this->prepareMessage($mailData);
        $sent = $this->mailer->send($this->message, $failedRecipients);
        if (!$sent) {
            $this->logger->error('failed to send emails to: ' . var_export($failedRecipients, true));
        }
    }

    /**
     * @param $mailData
     * @throws Exception
     */
    protected function prepareMessage($mailData)
    {
        try {
            $this->message = (new Swift_Message('Rejestracja WiP'))
                ->setFrom('no-reply@exmaple.org')
                ->setTo([
                    $mailData['email'] => $mailData['firstname'] . ' ' . $mailData['surname'],
                ])
                ->setBody(
                    $this->twig->render('emails/registration.html.twig', [
                        'email'          => $mailData['email'],
                        'firstname'      => $mailData['firstname'],
                        'surname'        => $mailData['surname'],
                        'description'    => $mailData['description'],
                        'position'       => $mailData['position']['name'],
                        'additionalData' => array_filter($mailData['additionalData'], function ($key) {
                            return $key !== 'id';
                        }, ARRAY_FILTER_USE_KEY),
                    ]),
                    'text/html'
                );
        } catch (Exception $e) {
            $this->logger->critical('wystapil blad generowania tresci mail - ' . $e->getMessage());
            throw new Exception('wystapil blad generowania tresci mail - ' . $e->getMessage());
        }
    }


}
