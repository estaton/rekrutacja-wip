# Instalacja
uruchomienie kontenerów Dockera:
```bash
docker-compose up -d
```
podniesienie zmian w DB można zrobić dopiero gdy kontener z MySQL w pełni się uruchomi
```bash
docker-compose run php php bin/console doctrine:migrations:migrate --no-interaction
```

Cały proces powinien trwać maksymalnie 10 minut - zależy jak szybko zaciągną się obrazy Dockera i zbudują zależności PHP i JS.

Uwaga! Ze względu na wydajność nie jest używana synchronizacja plików lokalnych w kontenerach php i node.

## Konfiguracja
W aplikacji w wersji prezentacyjnej nie ma nic do konfigurowania. Zarówno API jak i front działają w trybie produkcyjnym.

Aplikacje są skonfigurowane standardowo dla Symfony `api/config/` i Angulara `front/src/environments/`

Można ewentualnie skonfigurować w pliku `.env` porty Dockera dla systemu hostującego gdyby istniał konflikt z istniejącymi już w systemie (UWAGA! w przypadku zmiany portu API konieczna jest zmiana adresu w konfiguracji Angulara)

# Opis
Aplikacja została rodzielona na dwa systemy, frontowy w JavaScript/TypeScript oraz API w PHP.
## Dostępne adresy
* front
    * http://localhost/
* API
    * http://localhost:8080/
* Adminer
    * http://localhost:8081/
* MailHog
    * http://localhost:8082/

## Front
Aplikacja frontowa została przygotowana w Angular 7.2 oraz Bootstrap 4. Do wygenerowania szkieletu aplikacji użyłem generatora ngx-rocket. 

Front posiada obsługę JWT, uprawnienia do części administracyjnej są ograniczone dla zalogowanych użytkowników z rolą ROLE_ADMIN.

Formularz rejestracji konta posiada obsługą walidacji wbudowanej oraz błędów walidacji zwróconych przez API.

Dane logowania podstawowego użytkownika adminstracyjnego: `admin:admin`

## API
Do API idealnie by pasował `API Platform` jednak w jego przypadku niewiele już by było do sprawdzania pod kątem czytelności napisanego przeze mnie kodu.

API zostało przygotowane w Symfony 4.3 oraz wykorzystuje bazę danych MySQL. Komunikacja z bazą danych odbywa się przez Doctrine. Uwierzytelnienie odbywa się z JWT, system posiada obsługę ról.

Nazewnictwo metod kontrolera zrobiłem zgodnie z BREAD.

## DB
Struktura bazy danych jest zarządzana przez Doctrine poprzez `Doctrine Migrations`. Cześć struktury DB została uproszczona przez dziedziczenie encji (przykład: encja `Position` jest szczególnym przypadkiem encji `Dictionary`, czy `AuthUser` szczególnym przypadkiem `User`).

W bazie danych nie są używane ze względów bezpieczeństwa sekwencyjne identyfikatory numeryczne. Do tego celu używany jest generator UUID.

Tabela słownikowa `dictionary` posiada kolumnę `code`, której wartość jest niezmienna, co pozwala bardziej stabilnie odwoływać się do określonych pozycji (np. warunkowanie według wartości na froncie).

Aktualizacja bazy danych odbywa się w wersji developerskiej poprzez komendę
```bash
php bin/console doctrine:schema:update
```
lub w wersji produkcyjnej
```bash
php bin/console doctrine:migrations:migrate
```

Do bazy można też się do celów developerskich dostać poprzez Adminera, ustawienia:
```text
username: wip_user
password: wip_password
dbname: wip
```

## Mail
Do celów developerskich użyty został fake mail server MailHog.

## Logowanie
Dostępny jest jeden domyślny użytkownik techniczny admin:admin z rolą ROLE_ADMIN.

Specyfikacja mówiła o rejestracji kont, jednak nie przewidywała opcji hasła, założyłem więc wstępnie, że rejestracje te nie służą do logowania. Zastosowałem jednak rozwiązanie umożliwiające łatwe przerobienie na potrzeby faktycznego logowania. Wpisy w tabeli `user` mają zdefiniowany typ konta `type` poprzez wartości `user`/`authuser`.

